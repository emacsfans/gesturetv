# Introduction

This is a project to control TV by means of gestures. It is very experimental and is not assumed to be put into production.

# Dependency
- opencv
- boost

# Building 
Open your favorite terminal, go to the project root directory, then run the following commands:
```
mkdir build 
cd build
cmake ..
make
```

# Building TV GUI
Open your favorite terminal, go to the project root directory, then run the following commands:
```
qmake
make
```

# Run
After the compilation, you can invoke the CaptureReceiver by typing:
```
./CaptureReceiver
```
It will listen at localhost::1234.

Then open another terminal, and invoke the GestureTV to start the program by typing:
```
./GestureTV
```

# Run TV GUI
First start vlc rc remote server:
```
./start_vlc_remote.sh
```
Then run the TV GUI:
```
./gesturetv
```
