#ifndef _SIMULATETV_H_
#define _SIMULATETV_H_

#include <QWidget>

class TVScreen;
class ReceiverWorker;
class VLCControl;

class SimulateTV : public QWidget {
	Q_OBJECT
public:
	SimulateTV(QWidget *parent = 0);
public slots:
	void open();
	void close();
	void next();
	void prev();
	void pause();
	void play();
	void volumn_up();
	void volumn_down();
private:
	TVScreen *m_screen;
	ReceiverWorker *m_receiver;
	VLCControl *m_controller;
};

#endif /* _SIMULATETV_H_ */
