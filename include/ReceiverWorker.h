#ifndef _RECEIVERWORKER_H_
#define _RECEIVERWORKER_H_

#include <QtGui>
class ReceiverWorker : public QThread {
	Q_OBJECT
signals:
	void move(double x_in_percent, double y_in_percent);
public slots:
	void startListen();
};

#endif /* _RECEIVERWORKER_H_ */

