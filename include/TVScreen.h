#ifndef _TVSCREEN_H_
#define _TVSCREEN_H_

#include <QWidget>
#include <QLabel>
#include <QString>

const int CONTROL_REGION_NUM = 4;
const int TOP = 0;
const int BOTTOM = 1;
const int LEFT = 2;
const int RIGHT = 3;
const int NONE = 5;

class TVScreen : public QWidget {
	Q_OBJECT
public:
	TVScreen(QWidget *parent = 0);
	virtual void paintEvent(QPaintEvent *);
	virtual void mouseMoveEvent(QMouseEvent *event);
signals:
	void open();
	void close();
	void pause();
	void next();
	void prev();
	void play();
	void volumn_up();
	void volumn_down();
public slots:
	void find_move(double x, double y);
	void find_move_in_camera(double x_in_percent,
				 double y_in_percent);
private:
	bool match_sequence(const int *seq, 
			    int num = CONTROL_REGION_NUM);
	QRect m_rect_list[CONTROL_REGION_NUM];
	int m_rect_seq_list[CONTROL_REGION_NUM];
	QRect m_rect_center;
	QRect m_rect_info;
	void reset_rect_seq();
	void reset_rect_seq_and_disable_left();
	void reset_rect_seq_and_disable_right();
	int m_disable_region;
	int m_seq;

	double _x;
	double _y;

	double _hint_alpha;
	QString _hint_content;
};

#endif /* _TVSCREEN_H_ */
