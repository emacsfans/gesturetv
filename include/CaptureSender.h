#ifndef _CAPTURESENDER_H_
#define _CAPTURESENDER_H_

#include <string>
#include <boost/asio.hpp>

class CaptureSender {
public:
        static void send(const std::string &message);
private:
	static void send_handler(const boost::system::error_code &error,
		std::size_t byte_transfereed);
};

#endif /* _CAPTURESENDER_H_ */
