#ifndef _VLCCONTROL_H_
#define _VLCCONTROL_H_

#include <boost/asio.hpp>
#include <string>

using boost::asio::ip::tcp;
using namespace std;

class VLCControl {
public:
	int send_command(const string &command);
private:
};

#endif /* _VLCCONTROL_H_ */
