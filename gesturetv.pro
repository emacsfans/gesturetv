######################################################################
# Automatically generated by qmake (2.01a) Sun Nov 18 20:12:01 2012
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += . \
              include \
              src \

INCLUDEPATH += . include

# Input
HEADERS += include/SimulateTV.h \
           include/TVScreen.h \
           include/ReceiverWorker.h \
           include/VLCControl.h
           

SOURCES += src/SimulateTV.cpp \
           src/TVScreen.cpp \
           src/ReceiverWorker.cpp \
           src/VLCControl.cpp
LIBS += -lboost_system

OBJECTS_DIR = tmp
MOC_DIR = tmp

