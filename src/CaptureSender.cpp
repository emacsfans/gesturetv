#include "CaptureSender.h"

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <iostream>

using namespace std;
using namespace boost::asio;
using namespace boost::asio::ip;

void CaptureSender::send(const std::string &message) {
	try {
		io_service io_service;
		tcp::socket socket(io_service);
		tcp::endpoint tcp_endpoint(
			address::from_string("127.0.0.1"),
			1234);
		socket.connect(tcp_endpoint);
		cout << "Send" << endl;
//	socket.async_write_some(buffer(message), send_handler);
		socket.write_some(buffer(message));
	} catch (...) {
		cout << "Fail to send." << endl;
	}
}

void CaptureSender::send_handler(
	const boost::system::error_code &error,
	std::size_t byte_transfereed) {
	// do nothing
}
