#include <iostream>
#include <TLD.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

#include "VLCControl.h"

Rect box;
bool drawing_box = false;
bool gotBB = false;
bool tl = true;

void mouseHandler(int event, int x, int y, int flags, void *param)
{
	switch( event ) {
	case CV_EVENT_MOUSEMOVE:
		if (drawing_box) {
			box.width = x-box.x;
			box.height = y-box.y;
		}
		break;
	case CV_EVENT_LBUTTONDOWN:
		drawing_box = true;
		box = Rect( x, y, 0, 0 );
		break;
	case CV_EVENT_LBUTTONUP:
		drawing_box = false;
		if( box.width < 0 ){
			box.x += box.width;
			box.width *= -1;
		}
		if( box.height < 0 ){
			box.y += box.height;
			box.height *= -1;
		}
		gotBB = true;
		break;
	}
}

int main(int argc, char **argv)
{
        /* For debug */
	// VLCControl vlc;
	// cout << "Hello GestureTV!" << endl;
	// vlc.send_command("add /home/song/Downloads/love.mp3\r\n");
	// return 0;
	/* End */

	FileStorage fs;
	fs.open("../parameters.yml", FileStorage::READ);
	VideoCapture capture;
	capture.open(0);
	if (!capture.isOpened())
	{
		cout << "capture device failed to open!" << endl;
		return 1;
	} else {
		cout << "Capture opened" << endl;
	}
	cvNamedWindow("GestureTV", CV_WINDOW_AUTOSIZE);
	cvSetMouseCallback( "GestureTV", mouseHandler, NULL );
	capture.set(CV_CAP_PROP_FRAME_WIDTH,340);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT,240);
	
	Mat frame;
	Mat frame0;
	Mat last_gray;
	while (1) {
		capture >> frame0;
		flip(frame0, frame, 1);
		cvtColor(frame, last_gray, CV_RGB2GRAY);
		drawBox(frame,box);

		imshow("GestureTV", frame);
		if (cvWaitKey(33) == 'q')
			return 0;

		if (gotBB) {
			cvSetMouseCallback( "GestureTV", NULL, NULL );
			break;
		}
	}
	//Output file
	FILE  *bb_file = fopen("bounding_boxes.txt","w");
	TLD tld;
	tld.read(fs.getFirstTopLevelNode());
	tld.init(last_gray,box,bb_file);

	///Run-time
	Mat current_gray;
	BoundingBox pbox;
	vector<Point2f> pts1;
	vector<Point2f> pts2;
	bool status=true;
	int frames = 1;
	int detections = 1;

	while(capture.read(frame0)){
		flip(frame0, frame, 1);
		//get frame
		cvtColor(frame, current_gray, CV_RGB2GRAY);
		//Process Frame
		tld.processFrame(last_gray,current_gray,pts1,pts2,pbox,status,tl,bb_file);
		//Draw Points
		if (status){
			drawPoints(frame,pts1);
			drawPoints(frame,pts2,Scalar(0,255,0));
			drawBox(frame,pbox);
			detections++;
		}
		//Display
		imshow("GestureTV", frame);
		//swap points and images
		swap(last_gray,current_gray);
		pts1.clear();
		pts2.clear();
		frames++;
		printf("Detection rate: %d/%d\n",detections,frames);
		if (cvWaitKey(33) == 'q')
			break;
	}

	return 0;

}




