#include "TVScreen.h"

#include <QtGui>

#include <iostream>
using namespace std;

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int CONTROL_REGION_DISTANCE = 160;
const int CONTROL_REGION_SIZE = 70;
const int TOP_MARGIN = 30;
const int INFO_TOP_MARGIN = 200;
const int INFO_REGION_SIZE = 200;

const int OPEN_SEQUENCE[] = { 1, 3, 4, 2 };
const int CLOSE_SEQUENCE[] = { 1, 3, 2, 4 };
const int PAUSE_SEQUENCE[] = { 1, 0, 2, 0 };
const int PLAY_SEQUENCE[] = { 1, 0, 0, 2 };
const int NEXT_SEQUENCE[] = { 0, 0, 0, 1 };
const int PREV_SEQUENCE[] = { 0, 0, 1, 0 };
const int VOLUMN_UP_SEQUENCE[] = { 1, 0, 0, 0 };
const int VOLUMN_DOWN_SEQUENCE[] = { 0, 1, 0, 0 };

TVScreen::TVScreen(QWidget *parent)
	: QWidget(parent)
{
	m_rect_list[BOTTOM] = 
		QRect(0.5 * SCREEN_WIDTH, 
		      TOP_MARGIN + 0.5 * SCREEN_HEIGHT + CONTROL_REGION_DISTANCE, 
		      CONTROL_REGION_SIZE, CONTROL_REGION_SIZE);
	m_rect_list[TOP] = 
		QRect(0.5 * SCREEN_WIDTH, 
		      TOP_MARGIN + 0.5 * SCREEN_HEIGHT - CONTROL_REGION_DISTANCE, 
		      CONTROL_REGION_SIZE, CONTROL_REGION_SIZE);

	m_rect_list[LEFT] = 
		QRect(0.5 * SCREEN_WIDTH - CONTROL_REGION_DISTANCE, 
		      TOP_MARGIN + 0.5 * SCREEN_HEIGHT, 
		      CONTROL_REGION_SIZE, CONTROL_REGION_SIZE);
	m_rect_list[RIGHT] = 
		QRect(0.5 * SCREEN_WIDTH + CONTROL_REGION_DISTANCE,
		      TOP_MARGIN + 0.5 * SCREEN_HEIGHT, 
		      CONTROL_REGION_SIZE, CONTROL_REGION_SIZE);

	m_rect_center = 
		QRect(0.5 * SCREEN_WIDTH,
		      TOP_MARGIN + 0.5 * SCREEN_HEIGHT,
		      CONTROL_REGION_SIZE, CONTROL_REGION_SIZE);

	m_rect_info = 
		QRect(0.5 * SCREEN_WIDTH - INFO_REGION_SIZE * 7 / 20,
		      INFO_TOP_MARGIN - INFO_TOP_MARGIN * 1 / 4,
		      INFO_REGION_SIZE, INFO_REGION_SIZE);

	reset_rect_seq();

	setFixedWidth(SCREEN_WIDTH);
	setFixedHeight(SCREEN_HEIGHT);

	_x = 0;
	_y = 0;
	_hint_alpha = 0;
	_hint_content = "";
}

void TVScreen::reset_rect_seq()
{
	m_seq = 0;
	m_disable_region = NONE;
	for (int i = 0; i != CONTROL_REGION_NUM; ++i)
		m_rect_seq_list[i] = 0;
}

void TVScreen::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setPen(Qt::blue);
	painter.setFont(QFont("Arial", 30));
	painter.drawText(m_rect_center, Qt::AlignCenter, "TV");

	for (int i = 0; i != CONTROL_REGION_NUM; ++i) {
		if (m_rect_seq_list[i] != 0)
			painter.setPen(Qt::green);
		painter.drawEllipse(m_rect_list[i]);
		painter.setPen(Qt::blue);
	}

    QPixmap pix;
    pix.load("./hand.png");
    painter.drawPixmap(_x - 32, _y - 32, 64, 64, pix);
    int scale = 1.5;
    int picWidth = 93;
    int picHeight = 132;
    int divide = 5;
    painter.setFont(QFont("Arial", 10));

    pix.load("./open.png");
    painter.drawPixmap(0 * (picWidth/scale + divide), 0, picWidth/scale, picHeight/(scale + 0.5), pix);
    painter.drawText(0 * (picWidth/scale + divide), picHeight/(scale + 0.5), picWidth/scale, 50, Qt::AlignLeft|Qt::AlignVCenter, "Open.");

    pix.load("./close.png");
    painter.drawPixmap(1 * (picWidth/scale + divide), 0, picWidth/scale, picHeight/(scale + 0.5), pix);
    painter.drawText(1 * (picWidth/scale + divide), picHeight/(scale + 0.5), picWidth/scale, 50, Qt::AlignLeft|Qt::AlignVCenter, "Close.");

    pix.load("./play.png");
    painter.drawPixmap(2 * (picWidth/scale + divide), 0, picWidth/scale, picHeight/(scale + 0.5), pix);
    painter.drawText(2 * (picWidth/scale + divide), picHeight/(scale + 0.5), picWidth/scale, 50, Qt::AlignLeft|Qt::AlignVCenter, "Play.");

    pix.load("./pause.png");
    painter.drawPixmap(3 * (picWidth/scale + divide), 0, picWidth/scale, picHeight/(scale + 0.5), pix);
    painter.drawText(3 * (picWidth/scale + divide), picHeight/(scale + 0.5), picWidth/scale, 50, Qt::AlignLeft|Qt::AlignVCenter, "Pause.");

    pix.load("./next_channel.png");
    painter.drawPixmap(4 * (picWidth/scale + divide), 0, picWidth/scale, picHeight/(scale + 0.5), pix);
    painter.drawText(4 * (picWidth/scale + divide), picHeight/(scale + 0.5), picWidth/scale, 50, Qt::AlignLeft|Qt::AlignVCenter, "Next channel.");

    pix.load("./last_channel.png");
    painter.drawPixmap(5 * (picWidth/scale + divide), 0, picWidth/scale, picHeight/(scale + 0.5), pix);
    painter.drawText(5 * (picWidth/scale + divide), picHeight/(scale + 0.5), picWidth/scale, 50, Qt::AlignLeft|Qt::AlignVCenter, "Last channel.");

    pix.load("./up_volumn.png");
    painter.drawPixmap(6 * (picWidth/scale + divide), 0, picWidth/scale, picHeight/(scale + 0.5), pix);
    painter.drawText(6 * (picWidth/scale + divide), picHeight/(scale + 0.5), picWidth/scale, 50, Qt::AlignLeft|Qt::AlignVCenter, "Up volumn.");

    pix.load("./down_volumn.png");
    painter.drawPixmap(7 * (picWidth/scale + divide), 0, picWidth/scale, picHeight/(scale + 0.5), pix);
    painter.drawText(7 * (picWidth/scale + divide), picHeight/(scale + 0.5), picWidth/scale, 50, Qt::AlignLeft|Qt::AlignVCenter, "Down volumn.");

    if(_hint_alpha > 0)
        _hint_alpha = _hint_alpha - 0.01;
    painter.setOpacity(_hint_alpha);
    painter.setFont(QFont("Arial", 20));
    painter.setPen(Qt::darkGreen);
    painter.drawText(m_rect_info, Qt::AlignCenter, _hint_content);
//    painter.drawText(300, 150, 200, 200, Qt::AlignCenter, _hint_content);
    painter.setOpacity(1);
}

void TVScreen::mouseMoveEvent(QMouseEvent *event)
{
	find_move(event->x(), event->y());  
}

void TVScreen::find_move_in_camera(double x_in_percent,
				   double y_in_percent)
{
	find_move(x_in_percent * SCREEN_WIDTH,
		  y_in_percent * SCREEN_HEIGHT);
}

void TVScreen::find_move(double x, double y)
{
	cout << "FIND MOVE" << x << ", " << y << endl;
//update position in class
    _x = x;
    _y = y;
	update();
	for (int i = 0; i != CONTROL_REGION_NUM; ++i) {
		if (m_rect_seq_list[i] == 0 &&
		    m_disable_region != i &&
		    m_rect_list[i].contains(x, y)) {
			m_disable_region = NONE;
			++m_seq;
			m_rect_seq_list[i] = m_seq;
			update();
			if (match_sequence(OPEN_SEQUENCE)) {
				emit open();
				reset_rect_seq_and_disable_left();
                _hint_alpha = 1;
                _hint_content = "open";
			} else if (match_sequence(CLOSE_SEQUENCE)) {
				emit close();
				reset_rect_seq_and_disable_right();
                _hint_alpha = 1;
                _hint_content = "close";
			}
		}
	}
	if (m_rect_center.contains(x, y)) {
		if (match_sequence(PLAY_SEQUENCE)) {
			emit play();
            _hint_alpha = 1;
            _hint_content = "play";
		} else if (match_sequence(PAUSE_SEQUENCE)) {
			emit pause();
            _hint_alpha = 1;
            _hint_content = "pause";
		} else if (match_sequence(NEXT_SEQUENCE)) {
			emit next();
            _hint_alpha = 1;
            _hint_content = "next channel";
		} else if (match_sequence(PREV_SEQUENCE)) {
			emit prev();
            _hint_alpha = 1;
            _hint_content = "last channel";
		} else if (match_sequence(VOLUMN_UP_SEQUENCE)) {
			emit volumn_up();
            _hint_alpha = 1;
            _hint_content = "volumn up";
		} else if (match_sequence(VOLUMN_DOWN_SEQUENCE)) {
			emit volumn_down();
            _hint_alpha = 1;
            _hint_content = "volumn down";
		} else if (match_sequence(PAUSE_SEQUENCE)) {
			emit pause();
            _hint_alpha = 1;
            _hint_content = "pause";
		}
		reset_rect_seq();
		update();
	}
}

bool TVScreen::match_sequence(const int *seq, int num)
{
	for (int i = 0; i != num; ++i) {
		if (m_rect_seq_list[i] != seq[i]) {
			return 0;
		}
	}
	return 1;
}

void TVScreen::reset_rect_seq_and_disable_left()
{
	reset_rect_seq();
	m_disable_region = LEFT;
}

void TVScreen::reset_rect_seq_and_disable_right()
{
	reset_rect_seq();
	m_disable_region = RIGHT;
}

