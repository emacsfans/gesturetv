#include <string>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>

using namespace std;
using namespace boost::asio;
using namespace boost::asio::ip;

int main() {
	boost::array<char, 1024> buf;
	io_service io_service;
	tcp::acceptor acceptor(io_service, 
			       tcp::endpoint(tcp::v4(), 1234));
	for(;;) {
		tcp::socket socket(io_service);
		acceptor.accept(socket);
		boost::system::error_code ignored_error;
		socket.read_some(buffer(buf), ignored_error);
		cout << buf.data() << endl;
	}
}
