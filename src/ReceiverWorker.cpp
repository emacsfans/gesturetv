#include "ReceiverWorker.h"

#include <string>
#include<stdio.h>
#include <boost/asio.hpp>
#include <boost/array.hpp>

using namespace std;
using namespace boost::asio;
using namespace boost::asio::ip;

void ReceiverWorker::startListen()
{
	boost::array<char, 1024> buf;
	io_service io_service;
	tcp::acceptor acceptor(io_service, 
			       tcp::endpoint(tcp::v4(), 1234));
	cout << "Started Listening..." << endl;
	for(;;) {
		tcp::socket socket(io_service);
		acceptor.accept(socket);
		boost::system::error_code ignored_error;
		socket.read_some(buffer(buf), ignored_error);
		double x1, x2, y1, y2, confident;
		sscanf(buf.data(),
			       "%lf,%lf,%lf,%lf,%lf", 
			       &x1, &y1, &x2, &y2,
			       &confident);
//		cout << buf.data() << endl;
//		cout << x1 << " " << x2 << " " << y1 << " " << y2 << endl;
		emit move(x1, y1);

	}
}
