#include "SimulateTV.h"

#include <QtGui>

#include "TVScreen.h"
#include "ReceiverWorker.h"
#include "VLCControl.h"

SimulateTV::SimulateTV(QWidget *parent)
	: QWidget(parent)
{
	m_screen = new TVScreen();
	m_receiver = new ReceiverWorker();
	m_controller = new VLCControl();
        QLayout *layout = new QVBoxLayout();
	layout->addWidget(m_screen);
	setLayout(layout);
	setFixedWidth(800);
	setFixedHeight(600);
	QThread *workThread = new QThread(this);
	connect(workThread, SIGNAL(started()),
		m_receiver, SLOT(startListen()));
	connect(workThread, SIGNAL(finished()),
		m_receiver, SLOT(deleteLater()));
	
	connect(m_screen, SIGNAL(open()),
		this, SLOT(open()));
	connect(m_screen, SIGNAL(close()),
		this, SLOT(close()));
	connect(m_screen, SIGNAL(play()),
		this, SLOT(play()));
	connect(m_screen, SIGNAL(pause()),
		this, SLOT(pause()));
	connect(m_screen, SIGNAL(next()),
		this, SLOT(next()));
	connect(m_screen, SIGNAL(prev()),
		this, SLOT(prev()));
	connect(m_screen, SIGNAL(volumn_up()),
		this, SLOT(volumn_up()));
	connect(m_screen, SIGNAL(volumn_down()),
		this, SLOT(volumn_down()));
	connect(m_receiver, SIGNAL(move(double,double)),
		m_screen, SLOT(find_move_in_camera(double,double)));
	m_receiver->moveToThread(workThread);
	workThread->start();
}

void SimulateTV::open()
{
	cout << "Start TV..." << endl;
	// Network stream for audio and tv
	m_controller->send_command(
		"add http://comic.sjtu.edu.cn/vlc/pl_xspf.asp\r\n");
	// The first channel should be CCTV 1 with high resolution
	m_controller->send_command("goto 9\r\n");
	m_controller->send_command("");
}

void SimulateTV::close()
{
	cout << "Close TV..." << endl;
	m_controller->send_command("clear\r\n");
}

void SimulateTV::next()
{
	// to next channel for personal playing
	// jump over the channel for group playing
	m_controller->send_command("next\r\n");
	m_controller->send_command("next\r\n");
}

void SimulateTV::prev()
{
	// to next channel for personal playing
	// jump over the channel for group playing
	m_controller->send_command("prev\r\n");
	m_controller->send_command("prev\r\n");
}

void SimulateTV::pause()
{
	cout << "Pause" << endl;
	m_controller->send_command("pause\r\n");
}

void SimulateTV::play()
{
	m_controller->send_command("play\r\n");
}

void SimulateTV::volumn_up()
{
	m_controller->send_command("volup 1\r\n");
}

void SimulateTV::volumn_down()
{
	m_controller->send_command("voldown 1\r\n");
}

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	
	SimulateTV simulateTV;
	simulateTV.show();

	return app.exec();
}

