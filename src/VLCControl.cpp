#include "VLCControl.h"

#include <boost/array.hpp>
#include <iostream>

#define VLC_HOST "127.0.0.1"
#define VLC_PORT 8080


int VLCControl::send_command(const string &command)
{
	try {
		boost::asio::io_service io_service;
		boost::asio::ip::tcp::socket socket(io_service);
		boost::asio::ip::tcp::endpoint endpoint(
			boost::asio::ip::address::from_string(VLC_HOST), 
			VLC_PORT);
		socket.connect(endpoint);
	
		// Wait for a moment for VLC Server initialization
		boost::asio::deadline_timer t(io_service, boost::posix_time::seconds(1));
		t.wait();
		boost::system::error_code ignored_error;

		boost::array<char, 128> buf;
		size_t len = socket.read_some(boost::asio::buffer(buf), ignored_error);
		std::cout.write(buf.data(), len);
		boost::asio::write(socket, boost::asio::buffer(command), ignored_error);
		len = socket.read_some(boost::asio::buffer(buf), ignored_error);
		cout << len << endl;
		std::cout.write(buf.data(), len);
	} catch (...) {
		cout << "Warning: Command '" 
		     << command << "' failed to send." << endl;
	}
	return 0;
}
